package com.five9.configuration;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@ActiveProfiles("test")
public class ConfigurationServiceApplicationTests {

    @Autowired
    ApplicationContext context;


    @Test
    @DisplayName("Integration test that verifies whether spring context loaded properly.")
    void contextLoads() {
        assertNotNull(context);
    }

    @Test
    void testMain() { // just make the tool happy :(
        ConfigurationServiceApplication.main(new String[]{});
    }

}
