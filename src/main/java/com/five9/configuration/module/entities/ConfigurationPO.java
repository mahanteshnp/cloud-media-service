package com.five9.configuration.module.entities;

import com.five9.gcp.datastore.DataStoreEntity;
import java.sql.Date;
import lombok.Getter;
import lombok.Setter;
import org.springframework.cloud.gcp.data.datastore.core.mapping.Entity;
import org.springframework.data.annotation.Id;

@Entity(name = "recording_configuration")
public class ConfigurationPO
    implements DataStoreEntity {

    @Id
    @Getter
    @Setter
    private Long tenantId;

    @Getter
    @Setter
    private String storageRegion;

    @Getter
    @Setter
    private Long storageLimit;

    @Getter
    @Setter
    private Date retentionPeriod;

    @Getter
    @Setter
    private Date storageStartDate;

    @Getter
    @Setter
    private boolean isActive;

    @Getter
    @Setter
    private Date storageEndDate;

}
