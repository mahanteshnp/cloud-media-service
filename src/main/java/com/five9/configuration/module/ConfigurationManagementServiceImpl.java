package com.five9.configuration.module;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.five9.configuration.api.rest.dto.Configuration;
import com.five9.configuration.module.entities.ConfigurationPO;
import com.five9.configuration.module.repository.ConfigurationRepository;
import java.util.Collection;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Implementation of configuration management API.
 */
@Component
public class ConfigurationManagementServiceImpl implements ConfigurationManagementService {

    private final ConfigurationRepository configurationRepository;

    private final ObjectMapper objectMapper;

    /**
     * creates service layer object.
     */
    public ConfigurationManagementServiceImpl(@Autowired ConfigurationRepository configurationRepository,
        @Autowired ObjectMapper objectMapper) {
        this.configurationRepository = configurationRepository;
        this.objectMapper = objectMapper;

    }

    @Override
    public Configuration createConfiguration(Configuration configuration) {
        ConfigurationPO configurationPO = new ConfigurationPO();
        configurationPO.setTenantId(configuration.getTenantId());
        //Add logic to determine if it is active
        configurationPO.setActive(Boolean.TRUE);
        configurationPO.setStorageStartDate(configuration.getStorageStartDate());
        //Add logic to set retention period
        configurationPO.setRetentionPeriod(configuration.getStorageStartDate());

        return convert(configurationRepository.save(configurationPO));

    }

    @Override
    public Configuration getConfigurationById(Long id) {
        Optional<ConfigurationPO> list = configurationRepository.findById(id);
        ConfigurationPO configurationPO = list.get();
        return convert(configurationPO);
    }

    @Override
    public Collection<Configuration> getConfigurations() {
        Iterable<ConfigurationPO> iterable = configurationRepository.findAll();

        return null;
    }

    @Override
    public Configuration updateConfiguration(Configuration configuration) {
        return null;
    }

    @Override
    public void deleteConfigurations(Long id) {

    }

    private Configuration convert(ConfigurationPO configurationPO) {

        Configuration configuration = new Configuration();
        configuration.setTenantId(configurationPO.getTenantId());
        configuration.setStorageLimit(configurationPO.getStorageLimit());

        return configuration;
    }

}

