package com.five9.configuration.module;

import com.five9.configuration.api.rest.dto.Configuration;
import java.util.Collection;

/**
 * Protocol agnostic service definition. A application can have multiple of these matching to a EP or a gRPC call.
 * Applications exposing the API's will pick the required implementation of this service def as per requirements.
 */
public interface ConfigurationManagementService {

    /**
     * creates resource.
     * @param configuration
     *
     */
    Configuration createConfiguration(Configuration configuration);

    /**
     * Fetches resource.
     * @param id
     *
     */
    Configuration getConfigurationById(Long id);

    /**
     * Fetches resource collection.
     *
     */
    Collection<Configuration> getConfigurations();

    /**
     * updates resource.
     * @param configuration
     *
     */
    Configuration updateConfiguration(Configuration configuration);

    /**
     * deletes resource.
     * @param id
     *
     */
    void deleteConfigurations(Long id);
}

