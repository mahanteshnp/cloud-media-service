package com.five9.configuration.module.repository;

import com.five9.configuration.module.entities.ConfigurationPO;
import org.springframework.cloud.gcp.data.datastore.repository.DatastoreRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository interface that supports CRUD operations.
 */

@Repository
public interface ConfigurationRepository extends DatastoreRepository<ConfigurationPO, Long> {

}
