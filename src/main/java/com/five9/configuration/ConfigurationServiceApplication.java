package com.five9.configuration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gcp.data.datastore.repository.config.EnableDatastoreRepositories;

@SpringBootApplication
@EnableDatastoreRepositories(basePackages = {"com.five9.configuration.module.repository"})
public class ConfigurationServiceApplication {

    /**
     * Springboot application.
     *
     * @param args default arguments
     */
    public static void main(String[] args) {
        SpringApplication.run(ConfigurationServiceApplication.class, args);
    }

}
