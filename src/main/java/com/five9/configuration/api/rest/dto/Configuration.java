package com.five9.configuration.api.rest.dto;

import java.sql.Date;
import lombok.Getter;
import lombok.Setter;

/**
 * DTO representing a Configuration entity. Consuming apps can define their own or use the published artifact which will
 * have all the DTO's + Feign Clients.
 */
public class Configuration {

    @Getter
    @Setter
    private Long tenantId;

    @Getter
    @Setter
    private String storageRegion;

    @Getter
    @Setter
    private Long storageLimit;

    @Getter
    @Setter
    private Long retentionPeriod;

    @Getter
    @Setter
    private Date storageStartDate;

}

