package com.five9.configuration.api.rest.resources;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import com.five9.configuration.api.rest.dto.Configuration;
import lombok.Getter;
import org.springframework.hateoas.ResourceSupport;


/**
 * Creates hyperlink representing resource.
 */
@Getter
public class ConfigurationResource extends ResourceSupport {

    private final Configuration configuration;

    /**
     * Creates hyperlink.
     * @param configuration Resource Object
     */
    public ConfigurationResource(final Configuration configuration) {
        this.configuration = configuration;
        final long id = configuration.getTenantId();
        add(linkTo(ConfigurationController.class).withRel("/v1/storage/configurations"));
        add(linkTo(methodOn(ConfigurationController.class).getConfigurations()).withSelfRel());

    }
}
