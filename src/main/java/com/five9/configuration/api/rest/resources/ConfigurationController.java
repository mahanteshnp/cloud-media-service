package com.five9.configuration.api.rest.resources;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

import com.five9.configuration.api.rest.dto.Configuration;
import com.five9.configuration.module.ConfigurationManagementService;
import java.net.URI;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * Endpoint that performs CRUD operations on Configuration entity. This controller simply delegates to the actual
 * backend domain logic and usually should not contain any bus. logic. If domain/business logic throws errors for valid
 * business reasons, translate that to reasonable HTTP status code and stream the response.
 */

@RestController
@RequestMapping(path = "/v1/storage/configurations")
public class ConfigurationController {

    private ConfigurationManagementService configurationManagementService;

    ConfigurationController(@Autowired ConfigurationManagementService configurationManagementService) {
        this.configurationManagementService = configurationManagementService;
    }

    @GetMapping(produces = APPLICATION_JSON_UTF8_VALUE)
    //  @PreAuthorize("hasRole('Agent')")
    Collection<Configuration> getConfigurations() {
        return configurationManagementService.getConfigurations();
    }

    @GetMapping(value = "/{id}", produces = APPLICATION_JSON_UTF8_VALUE)
    Configuration getconfigurationById(@PathVariable Long id) {
        return configurationManagementService.getConfigurationById(id);
    }

    @PostMapping(consumes = APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity createConfiguration(@RequestBody Configuration configuration, UriComponentsBuilder uriBuilder) {

        Configuration createdConfiguration = configurationManagementService.createConfiguration(configuration);
        URI location =
            uriBuilder.path("v1/configurations/{id}").buildAndExpand(createdConfiguration.getTenantId()).toUri();

        final URI uri = MvcUriComponentsBuilder.fromController(getClass())
            .path("/{id}")
            .buildAndExpand(createdConfiguration.getTenantId())
            .toUri();

        return ResponseEntity.created(location).body(new ConfigurationResource(configuration));

    }

    @PutMapping(value = "/{id}", produces = APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity updateConfiguration(@RequestBody Configuration configuration, UriComponentsBuilder uriBuilder,
        @PathVariable Long id) {
        return null;
    }

    @DeleteMapping(value = "/{id}", produces = APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity deleteConfiguration(UriComponentsBuilder uriBuilder, @PathVariable Long id) {

        return null;
    }

}
