FROM gcr.io/google-appengine/openjdk:8

COPY build/libs/*.war app.war
ENV PORT="80"

CMD java -Dserver.port=$PORT -jar app.war
